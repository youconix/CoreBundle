<?php

namespace youconix\Core\Test\Html\Traits;

use PHPUnit\Framework\TestCase;
use youconix\Core\Html\Traits\RelationTrait;


class RelationTraitTest extends TestCase
{
    /**
     * @var RelationTrait
     */
    private $relation;

    public function setUp(): void
    {
        $this->relation = new class {
            use RelationTrait;

            public function generateItem()
            {
                return $this->relation;
            }
        };
    }

    /**
     * @test
     */
    public function setRelation(): void
    {
        $relation = 'test-relation';
        $this->relation->setRelation($relation);

        $expected = 'rel="'.$relation.'" ';
        $this->assertEquals($expected, $this->relation->generateItem());
    }
}