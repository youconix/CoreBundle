<?php

namespace youconix\Core\Test\Html\Traits;

use PHPUnit\Framework\TestCase;
use youconix\Core\Html\Traits\CssTrait;

class CssTraitTest extends TestCase
{
    /**
     * @var CssTrait
     */
    private $cssTrait;

    public function setUp(): void
    {
        $this->cssTrait = new class
        {
            use CssTrait;

            public function generateItem()
            {
                return $this->parseCss();
            }
        };
    }

    /**
     * @test
     */
    public function addClasses(): void
    {
        $classes = [
            'form-item',
            'select',
        ];

        foreach ($classes as $class) {
            $this->cssTrait->addClass($class);
        }

        $expected = 'class="' . implode(' ', $classes) . '" ';
        $this->assertEquals($expected, $this->cssTrait->generateItem());
    }

    /**
     * @test
     */
    public function setStyle(): void
    {
        $style = 'border:1px dotted green';

        $this->cssTrait->setStyle($style);

        $expected = 'style="' . $style . '" ';
        $this->assertEquals($expected, $this->cssTrait->generateItem());
    }
}