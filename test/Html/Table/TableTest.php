<?php

namespace youconix\Core\Test\Html;

use PHPUnit\Framework\TestCase;
use youconix\Core\Html\Html;
use youconix\Core\Html\Table\Table;
use youconix\Core\Html\Table\TableRow;

class TableTest extends TestCase
{
    /** @var Html */
    private $factory;

    /** @var Table $table */
    private $table;

    public function setUp(): void
    {
        $this->factory = new Html();
        $this->table = $this->factory->table();
    }

    /**
     * @test
     * @param TableRow[] $headers
     * @param TableRow[] $rows
     * @param TableRow[] $footers
     * @param string $expected
     * @dataProvider tableProvider
     */
    public function table(array $headers, array $rows, array $footers, $expected): void
    {
        foreach ($headers as $header) {
            $this->table->addHeaderRow($header);
        }
        foreach ($rows as $row) {
            $this->table->addRow($row);
        }
        foreach ($footers as $footer) {
            $this->table->addFooterRow($footer);
        }

        $this->assertEquals($expected, $this->table->generateItem());
    }

    /**
     * @test
     */
    public function tableCellWithColspan(): void
    {
        $colspan = 3;
        $cell = $this->table->createRow()->createCell()
            ->setColSpan($colspan);
        $expected = '<td colspan="' . $colspan . '" ></td>';

        $this->assertEquals($expected, $cell->generateItem());
    }

    /**
     * @test
     */
    public function tableCellWithRowSpan(): void
    {
        $rowspan = 2;
        $cell = $this->table->createRow()->createCell()
            ->setRowSpan($rowspan);
        $expected = '<td rowspan="' . $rowspan . '" ></td>';

        $this->assertEquals($expected, $cell->generateItem());
    }

    /**
     * @test
     */
    public function tableCellWithColspanAndRowSpan(): void
    {
        $colspan = 3;
        $rowspan = 2;
        $cell = $this->table->createRow()->createCell()
            ->setColSpan($colspan)
            ->setRowSpan($rowspan);
        $expected = '<td colspan="' . $colspan . '" rowspan="' . $rowspan . '" ></td>';

        $this->assertEquals($expected, $cell->generateItem());
    }

    /**
     * @return array
     */
    public function tableProvider(): array
    {
        $row = new TableRow();

        $headers = [
            (new TableRow())->addCell($row->createCell()->setContent('H1')),
            (new TableRow())->addCell($row->createCell()->setContent('H2'))
        ];

        $body = [
            (new TableRow())->addCell($row->createCell()->setContent('B1')),
            (new TableRow())->addCell($row->createCell()->setContent('B2'))
        ];

        $footer = [
            (new TableRow())->addCell($row->createCell()->setContent('F1')),
            (new TableRow())->addCell($row->createCell()->setContent('F2'))
        ];

        return [
            [[], [], [], $this->generateTable([], [], [])],
            [[], $body, [], $this->generateTable([], ['B1', 'B2'], [])],
            [$headers, $body, [], $this->generateTable(['H1', 'H2'], ['B1', 'B2'], [])],
            [$headers, $body, $footer, $this->generateTable(['H1', 'H2'], ['B1', 'B2'], ['F1', 'F2'])],
        ];
    }

    /**
     * @param array $headers
     * @param $rows
     * @param array $footers
     * @return string
     */
    private function generateTable(array $headers, array $rows, array $footers): string
    {
        $table = '<table >' . PHP_EOL;
        if (count($headers) > 0) {
            $table .= '<thead>' . PHP_EOL;

            foreach ($headers as $header) {
                $table .= '<tr >' . PHP_EOL . '<td >' . $header . '</td>' . PHP_EOL . '</tr>' . PHP_EOL;
            }

            $table .= '</thead>' . PHP_EOL;
        }

        $table .= '<tbody>' . PHP_EOL;
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $table .= '<tr >' . PHP_EOL . '<td >' . $row . '</td>' . PHP_EOL . '</tr>' . PHP_EOL;
            }
        } else {
            $table .= PHP_EOL;
        }
        $table .= '</tbody>' . PHP_EOL;

        if (count($footers) > 0) {
            $table .= '<tfoot>' . PHP_EOL;

            foreach ($footers as $footer) {
                $table .= '<tr >' . PHP_EOL . '<td >' . $footer . '</td>' . PHP_EOL . '</tr>' . PHP_EOL;
            }

            $table .= '</tfoot>' . PHP_EOL;
        }

        return $table . '</table>' . PHP_EOL;
    }
}