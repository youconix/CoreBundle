<?php

namespace youconix\Core\Test\Html\Header;

use PHPUnit\Framework\TestCase;
use youconix\Core\Html\Header\Javascript;
use youconix\Core\Html\Html;

class JavascriptTest extends TestCase
{
    /** @var Html */
    private $factory;

    /** @var Javascript */
    private $javascript;

    /** @var string */
    private $code;

    public function setUp(): void
    {
        $this->factory = new Html();
        $this->code = 'function hello(){  alert("test"); }';
        $this->javascript = $this->factory->javascript($this->code);
    }

    /**
     * @test
     */
    public function javascript(): void
    {
        $expected = '<script >' . PHP_EOL . '<!--' . PHP_EOL . $this->code . PHP_EOL . '//-->' . PHP_EOL . '</script>';

        $this->assertEquals($expected, $this->javascript->generateItem());
    }

    /**
     * @test
     */
    public function javascriptWithType(): void
    {
        $this->javascript->setType('text/javascript');
        $expected = '<script type="text/javascript">' . PHP_EOL . '<!--' . PHP_EOL . $this->code . PHP_EOL . '//-->' . PHP_EOL . '</script>';

        $this->assertEquals($expected, $this->javascript->generateItem());
    }
}