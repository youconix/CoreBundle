<?php

namespace youconix\Core\Test\Html\Header;

use PHPUnit\Framework\TestCase;
use youconix\Core\Html\Header\JavascriptLink;
use youconix\Core\Html\Html;

class JavascriptLinkTest extends TestCase
{
    /** @var Html */
    private $factory;

    /** @var JavascriptLink */
    private $link;

    /** @var string */
    private $url = 'http://example.com/test.js';

    public function setUp(): void
    {
        $this->factory = new Html();
        $this->link = $this->factory->javascriptLink($this->url);
    }

    /**
     * @test
     */
    public function javascriptLink(): void
    {
        $expected = '<script src="' . $this->url . '" ></script>';

        $this->assertEquals($expected, $this->link->generateItem());
    }

    /**
     * @test
     */
    public function javascriptLinkWithType(): void
    {
        $this->link->setType('text/javascript');
        $expected = '<script src="' . $this->url . '" type="text/javascript" ></script>';

        $this->assertEquals($expected, $this->link->generateItem());
    }
}