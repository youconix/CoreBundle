<?php

namespace youconix\Core\Test\HTML;

use PHPUnit\Framework\TestCase;
use youconix\Core\Html\Container;
use youconix\Core\Html\Html;
use youconix\Core\Html\Link;

class HtmlTest extends TestCase
{
    /** @var Html */
    private $factory;

    public function setUp(): void
    {
        $this->factory = new Html();
        error_reporting(E_ALL);
    }

    /**
     * @test
     */
    public function div(): void
    {
        $div = $this->factory->div();
        $expected = '<div ></div>';

        $this->assertEquals($expected, $div->generateItem());
    }

    /**
     * @test
     */
    public function paragraph(): void
    {
        $paragraph = $this->factory->paragraph();
        $expected = '<p ></p>';

        $this->assertEquals($expected, $paragraph->generateItem());
    }

    /**
     * @test
     */
    public function link(): void
    {
        $url = 'http://example.com';
        $value = 'example.com';
        $link = $this->factory->link($url, $value);

        $expected = '<a href="' . $url . '" >' . $value . '</a>';
        $this->assertEquals($expected, $link->generateItem());
    }

    /**
     * @test
     */
    public function image(): void
    {
        $url = 'http://example.com/image.png';
        $title = 'image';
        $alt = 'alt image';

        $image = $this->factory->image($url)->setTitle($title)->setAltText($alt);
        $expected = '<img src="' . $url . '" title="' . $title . '" alt="' . $alt . '" >';

        $this->assertEquals($expected, $image->generateItem());
    }

    /**
     * @test
     */
    public function canvas(): void
    {
        $canvas = $this->factory->canvas();
        $this->assertEquals('<canvas ></canvas>' . PHP_EOL, $canvas->generateItem());
    }
}
