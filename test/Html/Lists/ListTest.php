<?php

namespace youconix\Test\Html\Lists;

use PHPUnit\Framework\TestCase;
use youconix\Core\Html\Html;
use youconix\Core\Html\Lists\ListItem;
use youconix\Core\Html\Lists\NumberedList;
use youconix\Core\Html\Lists\UnNumberedList;

class ListTest extends TestCase
{
    /** @var Html */
    private $factory;

    public function setUp(): void
    {
        $this->factory = new Html();
        error_reporting(E_ALL);
    }

    /**
     * @test
     * @deprecated
     */
    public function unList(): void
    {
        $this->disableDeprecated();

        $this->assertInstanceOf(NumberedList::class, $this->factory->unList(true));

        $this->assertInstanceOf(UnNumberedList::class, $this->factory->unList(false));
    }

    /**
     * @test
     */
    public function numberedList(): void
    {
        $this->assertInstanceOf(NumberedList::class, $this->factory->numberedList());
    }

    /**
     * @test
     */
    public function unNumberedList(): void
    {
        $this->assertInstanceOf(UnNumberedList::class, $this->factory->unNumberedList());
    }

    /**
     * @test
     * @deprecated
     */
    public function listItem(): void
    {
        $this->disableDeprecated();

        $this->assertInstanceOf(ListItem::class, $this->factory->listItem(''));
    }

    /**
     * @test
     */
    public function addListItem(): void
    {
        $list = $this->factory->unNumberedList();
        $list->addItem($list->createItem('item 1'));
        $list->addItem($list->createItem('item 2'));
        $list->addItem($list->createItem('item 3'));

        $expected = "<ul >\n" .
            "<li >item 1</li>\n" .
            "<li >item 2</li>\n" .
            "<li >item 3</li>\n" .
            '</ul>';

        $this->assertEquals($expected, $list->generateItem());
    }

    private function disableDeprecated(): void
    {
        error_reporting(E_ALL & E_STRICT && ~E_USER_DEPRECATED);
    }
}
