<?php

namespace youconix\Core\Test\Html\Form;

use youconix\Core\Html\Form\Checkbox;
use PHPUnit\Framework\TestCase;
use youconix\Core\Html\Html;

class CheckboxTest extends TestCase
{
    /** @var Checkbox */
    private $checkbox;

    /** @var string */
    private $name = 'test';

    public function setUp(): void
    {
        $factory = new Html();
        $this->checkbox = $factory->checkbox($this->name);
    }

    /**
     * @test
     */
    public function checkboxChecked(): void
    {
        $this->checkbox->setChecked();

        $expected = '<input type="checkbox" name="' . $this->name . '" checked="checked" >';
        $this->assertEquals($expected, $this->checkbox->generateItem());
    }

    /**
     * @test
     */
    public function checkboxCheckedWithValue(): void
    {
        $value = 'checkbox value';
        $this->checkbox->setValue($value)->setChecked();

        $expected = '<input type="checkbox" name="' . $this->name . '" value="' . $value . '" checked="checked" >';
        $this->assertEquals($expected, $this->checkbox->generateItem());
    }

    /**
     * @test
     */
    public function checkboxNotChecked(): void
    {
        $expected = '<input type="checkbox" name="' . $this->name . '" >';
        $this->assertEquals($expected, $this->checkbox->generateItem());
    }
}
