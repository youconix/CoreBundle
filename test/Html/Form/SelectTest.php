<?php

namespace youconix\Core\Test\Html\Form;

use youconix\Core\Html\Form\Select;
use PHPUnit\Framework\TestCase;
use youconix\Core\Html\Html;

class SelectTest extends TestCase
{
    /** @var Select */
    private $select;

    /** @var string */
    private $name = 'test';

    public function setUp(): void
    {
        $factory = new Html();
        $this->select = $factory->select($this->name);
    }

    /**
     * @test
     */
    public function emptyList(): void
    {
        $expected = '<select name="' . $this->name . '" >' . PHP_EOL . PHP_EOL . '</select>';
        $this->assertEquals($expected, $this->select->generateItem());
    }

    /**
     * @test
     */
    public function optionsListWithoutValue(): void
    {
        $this->select
            ->addOption('option 1')
            ->addOption('option 2');

        $expected = '<select name="' . $this->name . '" >' . PHP_EOL .
            '<option value="option 1" >option 1</option>' . PHP_EOL .
            '<option value="option 2" >option 2</option>' . PHP_EOL .
            '</select>';

        $this->assertEquals($expected, $this->select->generateItem());
    }

    /**
     * @test
     */
    public function optionsListWithValue(): void
    {
        $this->select
            ->addOption('option 1', false, 1)
            ->addOption('option 2', false, 2);

        $expected = '<select name="' . $this->name . '" >' . PHP_EOL .
            '<option value="1" >option 1</option>' . PHP_EOL .
            '<option value="2" >option 2</option>' . PHP_EOL .
            '</select>';

        $this->assertEquals($expected, $this->select->generateItem());
    }

    /**
     * @test
     */
    public function optionsListWithValueSelected(): void
    {
        $this->select
            ->addOption('option 1', false, 1)
            ->addOption('option 2', true, 2);

        $expected = '<select name="' . $this->name . '" >' . PHP_EOL .
            '<option value="1" >option 1</option>' . PHP_EOL .
            '<option value="2" selected="selected">option 2</option>' . PHP_EOL .
            '</select>';

        $this->assertEquals($expected, $this->select->generateItem());
    }
}
