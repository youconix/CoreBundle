<?php
declare(strict_types=1);

namespace youconix\Core\Html\Header;

use youconix\Core\Html\HtmlItemInterface;
use youconix\Core\Html\Traits\JavascriptTrait;

class StylesheetLink implements HtmlItemInterface
{
    use JavascriptTrait;

    /** @var string */
    private $tag;

    /** @var string */
    private $media;

    /** @var string */
    private $type = '';


    /**
     * @param string $link
     */
    public function __construct(string $link)
    {
        $this->tag = '<link rel="stylesheet" href="' . $link . '" {type}{media}{between}>';
    }

    /**
     * @param string $media
     * @return StylesheetLink
     */
    public function setMedia(string $media): StylesheetLink
    {
        $this->media = 'media="' . $media . '" ';
        return $this;
    }

    /**
     * @param string $type
     * @return StylesheetLink
     */
    public function setType(string $type): StylesheetLink
    {
        $this->type = 'type="' . $type . '" ';
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->generateItem();
    }

    /**
     * @return string
     */
    public function generateItem(): string
    {
        $between = $this->parseJavaScript();

        return str_replace(
            ['{between}', '{type}', '{media}'],
            [$between, $this->type, $this->media],
            $this->tag
        );
    }
}