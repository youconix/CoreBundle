<?php
declare(strict_types=1);

namespace youconix\Core\Html\Header;

use youconix\Core\Html\HtmlItemInterface;
use youconix\Core\Html\Traits\JavascriptTrait;

class JavascriptLink implements HtmlItemInterface
{
    use JavascriptTrait;

    /** @var string */
    private $tag;

    /** @var string */
    private $type = '';


    /**
     * @param string $link
     */
    public function __construct(string $link)
    {
        $this->tag = '<script src="' . $link . '" {type}{between}></script>';
    }

    /**
     * @param string $type
     * @return JavascriptLink
     */
    public function setType(string $type): JavascriptLink
    {
        $this->type = 'type="' . $type . '" ';
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->generateItem();
    }

    /**
     * @return string
     */
    public function generateItem(): string
    {
        $between = $this->parseJavaScript();

        return str_replace(
            ['{between}', '{type}'],
            [$between, $this->type],
            $this->tag
        );
    }
}