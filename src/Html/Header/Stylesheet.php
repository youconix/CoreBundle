<?php
declare(strict_types=1);

namespace youconix\Core\Html\Header;

use youconix\Core\Html\HtmlItemInterface;

class Stylesheet implements HtmlItemInterface
{
	/** @var string */
	private $tag;

	/** @var string */
	private $type;

	/** @var string */
	private $css;

	public function __construct()
	{
		$this->tag = '<style {type}>' . PHP_EOL . '<!--' . PHP_EOL . '{css}' . PHP_EOL . '//-->' . PHP_EOL . '</style>';
	}

	/**
	 * @param string $type
	 * @return Stylesheet
	 */
	public function setType(string $type): Stylesheet
	{
		$this->type = 'type="' . $type . '"';
		return $this;
	}

	/**
	 * @param string $css
	 * @return Stylesheet
	 */
	public function setCss(string $css): Stylesheet
	{
		$this->css = $css;
		return $this;
	}

	/**
	 * @return string
	 */
	public function __toString(): string
	{
		return $this->generateItem();
	}

	/**
	 * @return string
	 */
	public function generateItem(): string
	{
		return str_replace(['{type}', '{css}'],
			[$this->type, $this->css],
			$this->tag
		);
	}
}