<?php
declare(strict_types=1);

namespace youconix\Core\Html\Header;

use youconix\Core\Html\HtmlItemInterface;
use youconix\Core\Html\Traits\JavascriptTrait;

class Metatag implements HtmlItemInterface
{
    use JavascriptTrait;

    /** @var string */
    private $tag;

    /** @var string */
    private $scheme;

    /**
     * @param string $name
     * @param string $content
     */
    public function __construct(string $name, string $content)
    {
        $pre = in_array($name, ['refresh', 'charset', 'expires']) ? 'http-equiv' : 'name';

        $this->tag = '<meta {scheme}' . $pre . '="' . $name . '" content="' . $content . '" {between}>' . PHP_EOL;
    }

    /**
     * @param string $scheme
     * @return Metatag
     */
    public function setScheme(string $scheme): Metatag
    {
        if (!empty($scheme)) {
            $this->scheme = 'scheme="' . $scheme . '" ';
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->generateItem();
    }

    /**
     * @return string
     */
    public function generateItem(): string
    {
        $between = $this->parseJavaScript();

        return str_replace(['{between}', '{scheme}'],
            [$between, $this->scheme],
            $this->tag
        );
    }
}