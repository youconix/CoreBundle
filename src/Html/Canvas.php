<?php
declare(strict_types=1);

namespace youconix\Core\Html;

use youconix\Core\Html\Traits\CssTrait;
use youconix\Core\Html\Traits\JavascriptTrait;

class Canvas implements HtmlItemInterface
{
    use JavascriptTrait;
    use CssTrait;

    /** @var string */
    private $tag;

    public function __construct()
    {
        $this->tag = '<canvas {between}></canvas>' . PHP_EOL;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->generateItem();
    }

    /**
     * @return string
     */
    public function generateItem(): string
    {
        $between = $this->parseJavaScript() .
            $this->parseCss();

        return str_replace(
            ['{between}'],
            [$between],
            $this->tag
        );
    }
}