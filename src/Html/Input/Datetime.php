<?php
declare(strict_types=1);

namespace youconix\Core\Html\Input;

class Datetime extends Date
{
    /**
     * @param string $name
     * @param bool $localize
     */
    public function __construct(string $name, bool $localize)
    {
        if ($localize) {
            $this->tag = '<input type="datetime-local" name="'.$name.'" value="{value}" {min}{max}{between}>' . PHP_EOL;
        } else {
            $this->tag = '<input type="datetime" name="'.$name.'" value="{value}" {min}{max}{between}>' . PHP_EOL;
        }
    }
}