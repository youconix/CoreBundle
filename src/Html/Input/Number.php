<?php
declare(strict_types=1);

namespace youconix\Core\Html\Input;

class Number extends Range
{
    /**
     * @var string
     */
    private $step = '';

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->tag = '<input type="number" name="' . $name . '" value="{value}" {min}{max}{step}{between}>' . PHP_EOL;
    }
    
    /**
     * Sets the step size
     *
     * @param int|float $step
     * @return Number
     * @throws \InvalidArgumentException
     */
    public function setStep($step): Number
    {
        if (!is_numeric($step)) {
            throw new \InvalidArgumentException('The step of a Number should be an int or float.');
        }

        $this->step = 'step="' . $step . '" ';
        return $this;
    }

    /**
     * @return  string
     */
    public function generateItem(): string
    {
        $tag = parent::generateItem();

        return str_replace(
            ['{step}'],
            [$this->step],
            $tag
        );
    }
}