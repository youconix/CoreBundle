<?php
declare(strict_types=1);

namespace youconix\Core\Html\Lists;

use youconix\Core\Html\HtmlItemInterface;
use youconix\Core\Html\Traits\CssTrait;
use youconix\Core\Html\Traits\JavascriptTrait;

class UnNumberedList implements HtmlItemInterface
{
    use JavascriptTrait;
    use CssTrait;

    /** @var string */
    protected $tag;

    /** @var Container[] */
    protected $items = [];

    public function __construct()
    {
        $this->tag = '<ul {between}>'.PHP_EOL.'{items}'.PHP_EOL.'</ul>';
    }

    /**
     * @param string $content
     * @return ListItem
     */
    public function createItem(string $content = ''): ListItem
    {
        return (new ListItem())->setContent($content);
    }

    /**
     * @param ListItem $item
     * @return $this
     */
    public function addItem(ListItem $item): UnNumberedList
    {
        $this->items[] = $item;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->generateItem();
    }

    /**
     * @return string
     */
    public function generateItem(): string
    {
        $between = $this->parseJavaScript() .
            $this->parseCss();

        $items = [];
        foreach($this->items as $item) {
            $items[] = $item->generateItem();
        }

        return str_replace(
            ['{between}', '{items}'],
            [$between, implode(PHP_EOL, $items)],
            $this->tag
        );
    }
}