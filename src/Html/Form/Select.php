<?php
declare(strict_types=1);

namespace youconix\Core\Html\Form;

use youconix\Core\Html\HtmlItemInterface;
use youconix\Core\Html\Traits\CssTrait;
use youconix\Core\Html\Traits\JavascriptTrait;

class Select implements HtmlItemInterface
{
    use JavascriptTrait;
    use CssTrait;

    /** @var string */
    private $tag;

    /** @var array */
    private $options = [];

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->tag = '<select name="' . $name . '" {between}>' . PHP_EOL . '{options}' . PHP_EOL . '</select>';
    }

    /**
     * @param string $value
     * @param bool $isSelected
     * @param string $hiddenValue
     * @return Select
     * @deprecated Use addOption instead
     */
    public function setOption(string $value, bool $isSelected, string $hiddenValue = ''): Select
    {
        return $this->addOption($value, $isSelected, $hiddenValue);
    }

    /**
     * @param string $value
     * @param bool $isSelected
     * @param string $hiddenValue
     * @return Select
     */
    public function addOption(string $value, bool $isSelected = false, string $hiddenValue = ''): Select
    {
        $selected = $isSelected ? 'selected="selected"' : '';
        $hiddenValue = !empty($hiddenValue) ? $hiddenValue : $value;

        $this->options[] = '<option value="' . $hiddenValue . '" ' . $selected . '>' . $value . '</option>';

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->generateItem();
    }

    /**
     * @return string
     */
    public function generateItem(): string
    {
        $between = $this->parseJavaScript() .
            $this->parseCss();

        return str_replace(
            ['{between}', '{options}'],
            [$between, implode(PHP_EOL, $this->options)],
            $this->tag
        );
    }
}