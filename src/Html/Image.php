<?php
declare(strict_types=1);

namespace youconix\Core\Html;

use youconix\Core\Html\Traits\CssTrait;
use youconix\Core\Html\Traits\JavascriptTrait;

class Image implements HtmlItemInterface
{
    use JavascriptTrait;
    use CssTrait;

    /** @var string */
    private $tag;

    /** @var string */
    private $altText = '';

    /** @var string */
    private $title = '';

    /**
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->tag = '<img src="' . $url . '" {title}{altText}{between}>';
    }

    /**
     * @param string $title
     * @return Image
     */
    public function setTitle(string $title): Image
    {
        $this->title = 'title="' . $title . '" ';
        return $this;
    }

    /**
     * @param string $altText
     * @return Image
     */
    public function setAltText(string $altText): Image
    {
        $this->altText = 'alt="' . $altText . '" ';
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->generateItem();
    }

    /**
     * @return string
     */
    public function generateItem(): string
    {
        $between = $this->parseJavaScript() .
            $this->parseCss();

        return str_replace(
            ['{title}', '{altText}', '{between}'],
            [$this->title, $this->altText, $between],
            $this->tag
        );
    }
}