<?php

/**
 * General class loader
 *
 * @link        http://www.php-fig.org/psr/psr-4/
 */
final class Autoload
{
    /** @var Autoload */
    private static $instance;

    /** @var string */
    private $coreDir;

    /** @var string */
    private $projectDir;

    private function __construct()
    {
        $this->detectCoreDir();
        $this->detectProjectDir();
        $this->defineConstants();
    }

    /**
     * @return Autoload
     */
    public static function getInstance(): Autoload
    {
        if (is_null(self::$instance)) {
            self::$instance = new Autoload();
        }

        return self::$instance;
    }

    private function detectCoreDir()
    {
        $reflection = new \ReflectionObject($this);
        $this->coreDir = dirname($reflection->getFileName());
    }

    private function detectProjectDir()
    {
        $reflection = new \ReflectionObject($this);
        $dir = dirname($reflection->getFileName());
        while (!file_exists($dir . '/public/index.php')) {
            $dir = dirname($dir);
        }
        $this->projectDir = $dir;
    }

    private function defineConstants()
    {
        if (!defined('CORE')) {
            define('CORE', $this->coreDir);
        }
        if (!defined('WEB_ROOT')) {
            define('WEB_ROOT', $this->projectDir);
        }
        if (!defined('DS')) {
            define('DS', DIRECTORY_SEPARATOR);
        }
    }

    /**
     * @param string $className
     * @return null|string
     */
    private function getFileName(string $className): ?string
    {
        // Check for legacy classes
        $map = $this->classMap();
        if (in_array($className, $map))
        {
            $className = $map[$className];
        }

        $className = ltrim($className, '\\');
        $fileName = '';
        if ($lastNsPos = strrpos($className, '\\')) {
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos + 1);
            $fileName = str_replace('\\', DS, $namespace) . DS;
            $fileName = preg_replace('#youconix/([^/]+)/#si', 'youconix/${1}/src/', $fileName);
        }

        if (file_exists($this->projectDir . $fileName . DS . $className . '.inc.php')) {
            return $fileName . DS . $className . '.inc.php';
        }


        return null;
    }

    /**
     * @param string $className
     */
    public function loadClass(string $className): void
    {
        $fileName = $this->getFileName($className);

        if (!is_null($fileName)) {
            $path = $this->getProjectDir() . DS . $fileName;
            require $path;
        }
    }

    /**
     * @return string
     */
    public function getCoreDir(): string
    {
        return $this->coreDir;
    }

    /**
     * @return string
     */
    public function getProjectDir(): string
    {
        return $this->projectDir;
    }

    private function classMap()
    {
        $baseClassName = 'youconix\Core\Bridge\\';

        return [
            'Queue' => $baseClassName.'Classes\Queue',
            'Stack' => $baseClassName.'Classes\Stack',
            'String' => $baseClassName.'Classes\StringLegancy',

            'Builder_mysqli' => $baseClassName.'Database\Builder_mysqli',
            'Database_Mysqli' => $baseClassName.'Database\Database_Mysqli',
            'Database_Mysqli_binded' => $baseClassName.'Database\Database_Mysqli_binded',

            'DateException' => $baseClassName.'Exceptions\DateException',
            'DBException' => $baseClassName.'Exceptions\DBException',
            'GeneralException' => $baseClassName.'Exceptions\GeneralException',
            'IllegalArgumentException' => $baseClassName.'Exceptions\IllegalArgumentException',
            'IOException' => $baseClassName.'Exceptions\IOException',
            'MemoryException' => $baseClassName.'Exceptions\MemoryException',
            'NullPointerException' => $baseClassName.'Exceptions\NullPointerException',
            'TemplateException' => $baseClassName.'Exceptions\TemplateException',
            'TypeException' => $baseClassName.'Exceptions\TypeException',
            'ValidationException' => $baseClassName.'Exceptions\ValidationException',
            'XmlException' => $baseClassName.'Exceptions\XmlException',

            'HTML' => $baseClassName.'HTML\Html',
            ];



        if ($className == 'Service' || substr($className, 0, 8) === 'Service_')
        {
            $className = $baseClassName.'Services\\'.str_replace('Service_','', $className);
        }
        elseif ($className == 'Model' || substr($className, 0, 6) === 'Model_')
        {
            $className = $baseClassName.'Models\\'.str_replace('Models_','', $className);
        }
        elseif (substr($className, 0, 5) === 'Data_')
        {
            $className = $baseClassName.'Models\Data\\'.str_replace('Data_','', $className);
        };
    }
}

/**
 * @param string $className
 */
function loaderWrapper(string $className)
{
    Autoload::getInstance()->loadClass($className);
}

spl_autoload_register('loaderWrapper');
