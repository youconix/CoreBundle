<?php

namespace youconix\Core\Auth\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use youconix\Core\Auth\Entity\Login;
use youconix\Core\Auth\UserInterface;

class LoginRepository extends EntityRepository implements UserLoaderInterface
{
    /**
     * @param string $username
     * @return Login|null
     * @throws NonUniqueResultException
     */
    public function loadUserByUsername($username): ?Login
    {
        return $this->createQueryBuilder('l')
            ->where('l.username = :username and activated = "1" and blocked = "0"')
            ->setParameter('username', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param UserInterface $user
     * @param string $password
     */
    public function updatePassword(UserInterface $user, string $password): void
    {
        $this->createQueryBuilder('l')
            ->update('l.password = :password')
            ->where('l.id = :id')
            ->setParameter('password', $password)
            ->setParameter('id', $user->getID())
            ->getQuery()
            ->execute();

        $user->setPassword($password);
    }
}