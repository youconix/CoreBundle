<?php

namespace Youconix\Core\Auth;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use youconix\Core\Auth\Repository\LoginRepository;
use youconix\Core\Auth\Repository\RoleRepository;

class TokenAuthenticator extends AbstractGuardAuthenticator
{
    /** @var LoginRepository */
    private $loginRepository;

    /** @var EncoderFactoryInterface */
    private $passwordEncoderFactory;

    /** @var string */
    private $legacySiteSalt;

    /**
     * @param LoginRepository $loginRepository
     * @param EncoderFactoryInterface $passwordEncoderFactory
     * @param string $legacySiteSalt
     */
    public function __construct(
        LoginRepository $loginRepository,
        EncoderFactoryInterface $passwordEncoderFactory,
        string $legacySiteSalt
    )
    {
        $this->loginRepository = $loginRepository;
        $this->passwordEncoderFactory = $passwordEncoderFactory;
        $this->legacySiteSalt = $legacySiteSalt;
    }

    /**
     * @inheritDoc
     */
    public function start(Request $request, AuthenticationException $authException = null): Response
    {
        $data = [
            'message' => 'Authentication Required'
        ];

        return new Response($data, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @inheritDoc
     */
    public function supports(Request $request): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getCredentials(Request $request): array
    {
        return [
            'username' => $request->get('username', ''),
            'password' => $request->get('password', ''),
        ];
    }

    /**
     * @inheritDoc
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        return $this->loginRepository->loadUserByUsername($credentials['username']);
    }

    /**
     * @inheritDoc
     */
    public function checkCredentials($credentials, UserInterface $user): bool
    {
        $passwordEncoder = $this->passwordEncoderFactory->getEncoder($user);

        if (!$this->login($passwordEncoder, $credentials['password'], $user)) {
            if (!$this->legacyLogin($passwordEncoder, $credentials['password'], $user)) {
                return false;
            }

            $password = $passwordEncoder->encodePassword($credentials['password']);
            $this->loginRepository->updatePassword($user, $password);
        }

        $user->eraseCredentials();

        return true;
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new Response($data, Response::HTTP_FORBIDDEN);
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): ?Response
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function supportsRememberMe(): bool
    {
        return false;
    }

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param string $password
     * @param UserInterface $user
     * @return bool
     */
    private function login(UserPasswordEncoderInterface $passwordEncoder, string $password, UserInterface $user): bool
    {
        return $passwordEncoder->isPasswordValid($user, $password);
    }

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param string $password
     * @param UserInterface $user
     * @return bool
     */
    private function legacyLogin(UserPasswordEncoderInterface $passwordEncoder, string $password, UserInterface $user): bool
    {
        $oldHash = sha1(substr(md5($user->getUsername()), 5, 30) . $password . $this->legacySiteSalt);

        return $this->login($passwordEncoder, $oldHash, $user);
    }
}