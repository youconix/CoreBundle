<?php

namespace Youconix\Core\Auth;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

class Token extends AbstractToken
{
    /**
     * @inheritDoc
     */
    public function __construct(array $roles = [])
    {
        parent::__construct($roles);

        $this->setAuthenticated(count($roles) > 0);
    }

    /**
     * Returns the user credentials.
     *
     * @return string The user credentials
     */
    public function getCredentials()
    {
        return '';
    }
}