<?php/**
 * Created by PhpStorm.
 * User: rachelle
 * Date: 23-12-18
 * Time: 12:24
 */

namespace youconix\Core\Database;

interface Where {
    /**
     * Resets the class Where
     */
    public function reset();

    /**
     * Adds fields with an and relation
     *
     * @param		array $a_fields		The fields,also accepts a single value
     * @param		array	$a_types	The value types : l (SQL, no parse), i (int) ,d (double) ,s (string) or b (blob), also accepts a single value
     * @param		array	$a_values		The values, also accepts a single value
     * @param array		$a_keys			The keys (=|<>|<|>|LIKE|IN|BETWEEN), also accepts a single value. leave empty for =
     * @throws DBException		If the key is invalid
     */
    public function addAnd($a_fields,$a_types,$a_values,$a_keys);

    /**
     * Adds fields with an or relation
     *
     * @param		array $a_fields		The fields,also accepts a single value
     * @param		array	$a_types	The value types : l (SQL, no parse), i (int) ,d (double) ,s (string) or b (blob), also accepts a single value
     * @param		array	$a_values		The values, also accepts a single value
     * @param array		$a_keys			The keys (=|<>|<|>|LIKE|IN|BETWEEN), also accepts a single value. leave empty for =
     * @throws DBException		If the key is invalid
     */
    public function addOr($a_fields,$a_types,$a_values,$a_keys);

    /**
     * Starts a sub where part
     */
    public function startSubWhere();

    /**
     * Ends a sub where part
     */
    public function endSubWhere();

    /**
     * Adds a sub query
     *
     * @param		Builder	$obj_builder	The builder object
     * @param 	String	$s_field			The field
     * @param 	String	$s_key				The key (=|<>|LIKE|IN|BETWEEN)
     * @param		String 	$s_command		The command (AND|OR)
     * @throws DBException		If the key is invalid
     * @throws DBException		If the command is invalid
     */
    public function addSubQuery($obj_builder,$s_field,$s_key,$s_command);

    /**
     * Renders the where
     *
     * @return array		The where
     */
    public function render();
}