<?php

namespace youconix\Core\Database;

/**
 * @deprecated
 */
class Create_Mysqli implements Create
{
    private $s_query;
    private $a_createRows;
    private $a_createTypes;
    private $s_engine;
    private $s_dropTable;

    /**
     * @inheritdoc
     */
    public function reset()
    {
        trigger_error('Class Create_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        $this->s_query = '';
        $this->a_createRows = array();
        $this->a_createTypes = array();
        $this->s_engine = '';
        $this->s_dropTable = '';
    }

    /**
     * @inheritdoc
     */
    public function setTable($s_table, $bo_dropTable = false)
    {
        trigger_error('Class Create_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        if ($bo_dropTable) {
            $this->s_dropTable = 'DROP TABLE IF EXISTS ' . DB_PREFIX . $s_table;
        }

        $this->s_query = "CREATE TABLE " . DB_PREFIX . $s_table . " (";

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addRow(
        $s_field,
        $s_type,
        $i_length = -1,
        $s_default = '',
        $bo_signed = true,
        $bo_null = false,
        $bo_autoIncrement = false
    ) {
        trigger_error('Class Create_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        ($bo_signed) ? $s_signed = ' SIGNED ' : $s_signed = ' UNSIGNED ';

        $s_null = $this->checkNull($bo_null);
        if ($bo_null && $s_default == "") {
            $s_default = ' DEFAULT NULL ';
        } else {
            if ($s_default != "") {
                $s_default = " DEFAULT '" . $s_default . "' ";
            }
        }

        ($bo_autoIncrement) ? $s_autoIncrement = ' AUTO_INCREMENT' : $s_autoIncrement = '';
        $s_type = strtoupper($s_type);

        if (in_array($s_type, array('VARCHAR', 'SMALLINT', 'MEDIUMINT', 'INT', 'BIGINT'))) {
            $this->a_createRows[$s_field] = $s_field . ' ' . strtoupper($s_type) . '(' . $i_length . ') ' . $s_default . $s_null . $s_autoIncrement;
        } else {
            if ($s_type == 'DECIMAL') {
                $this->a_createRows[$s_field] = $s_field . ' DECIMAL(10,0) ' . $s_default . $s_null . $s_autoIncrement;
            } else {
                $this->a_createRows[$s_field] = $s_field . ' ' . strtoupper($s_type) . ' ' . $s_default . $s_null . $s_autoIncrement;
            }
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addEnum($s_field, $a_values, $s_default, $bo_null = false)
    {
        trigger_error('Class Create_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        $a_valuesPre = array();
        foreach ($a_values AS $s_value) {
            $a_valuesPre[] = "'" . $s_value . "'";
        }

        $s_null = $this->checkNull($bo_null);
        if ($bo_null && empty($s_default)) {
            $s_default = ' DEFAULT NULL ';
        } else {
            $s_default = " DEFAULT '" . $s_default . "' ";
        }

        $this->a_createRows[$s_field] = $s_field . ' ENUM(' . implode(',', $a_valuesPre) . ') ' . $s_default . $s_null;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addSet($s_field, $a_values, $s_default, $bo_null = false)
    {
        trigger_error('Class Create_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        $a_valuesPre = array();
        foreach ($a_values AS $s_value) {
            $a_valuesPre[] = "'" . $s_value . "'";
        }

        $s_null = $this->checkNull($bo_null);
        if ($bo_null && empty($s_default)) {
            $s_default = ' DEFAULT NULL ';
        } else {
            $s_default = " DEFAULT '" . $s_default . "' ";
        }

        $this->a_createRows[$s_field] = $s_field . ' SET(' . implode(',', $a_valuesPre) . ') ' . $s_default . $s_null;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addPrimary($s_field)
    {
        trigger_error('Class Create_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        if (!array_key_exists($s_field, $this->a_createRows)) {
            throw new DBException("Can not add primary key on unknown field $s_field.");
        }
        if (array_key_exists('primary', $this->a_createTypes)) {
            throw new DBException("Only one primary key pro table is allowed.");
        }

        $this->a_createTypes['primary'] = 'PRIMARY KEY (' . $s_field . ')';

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addIndex($s_field)
    {
        trigger_error('Class Create_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        if (!array_key_exists($s_field, $this->a_createRows)) {
            throw new DBException("Can not add index key on unknown field $s_field.");
        }

        $this->a_createTypes[] = 'KEY ' . $s_field . ' (' . $s_field . ')';

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addUnique($s_field)
    {
        trigger_error('Class Create_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        if (!array_key_exists($s_field, $this->a_createRows)) {
            throw new DBException("Can not add unique key on unknown field $s_field.");
        }

        $this->a_createTypes[] = 'UNIQUE KEY ' . $s_field . ' (' . $s_field . ')';

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addFullTextSearch($s_field)
    {
        trigger_error('Class Create_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        if (!array_key_exists($s_field, $this->a_createRows)) {
            throw new DBException("Can not add full text search on unknown field $s_field.");
        }
        if (stripos($this->a_createRows[$s_field], 'VARCHAR') === false && stripos($this->a_createRows[$s_field],
                'TEXT') === false) {
            throw new DBException("Full text search can only be added on VARCHAR or TEXT fields.");
        }

        $this->a_createTypes[] = 'FULLTEXT KEY ' . $s_field . ' (' . $s_field . ')';

        $this->s_engine = 'ENGINE=MyISAM';

        return $this;
    }

    /**
     * Parses the null setting
     *
     * @param boolean $bo_null The null setting
     * @return String        The null text
     */
    private function checkNull($bo_null)
    {
        $s_null = ' NOT NULL ';
        if ($bo_null) {
            $s_null = ' NULL ';
        }

        return $s_null;
    }

    /**
     * @inheritdoc
     */
    public function getDropTable()
    {
        trigger_error('Class Create_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        return $this->s_dropTable;
    }

    /**
     * @inheritdoc
     */
    public function render()
    {
        trigger_error('Class Create_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        $this->s_query .= "\n" . implode(",\n", $this->a_createRows);
        if (count($this->a_createTypes) > 0) {
            $this->s_query .= ",\n";
            $this->s_query .= implode(",\n", $this->a_createTypes);
        }
        $this->s_query .= "\n)" . $this->s_engine;

        return $this->s_query;
    }
}