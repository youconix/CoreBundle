<?php

namespace youconix\Core\Database;

use youconix\Core\Bridge\Exceptions\DBException;

/**
 * @deprecated
 */
abstract class QueryConditions_Mysqli
{
    protected $s_query;
    protected $a_types;
    protected $a_values;
    protected $a_keys = array(
        '=' => '=',
        '==' => '=',
        '<>' => '<>',
        '!=' => '<>',
        '<' => '<',
        '>' => '>',
        'LIKE' => 'LIKE',
        'IN' => 'IN',
        'BETWEEN' => 'BETWEEN'
    );

    /**
     * @inheritdoc
     */
    public function reset()
    {
        trigger_error('Class QueryConditions_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        $this->s_query = '';
        $this->a_types = array();
        $this->a_values = array();
    }

    /**
     * @inheritdoc
     */
    public function addAnd($a_fields, $a_types, $a_values, $a_keys = array())
    {
        trigger_error('Class QueryConditions_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        if (!is_array($a_fields)) {
            $a_fields = array($a_fields);
        }
        if (!is_array($a_types)) {
            $a_types = array($a_types);
        }
        if (!is_array($a_values)) {
            $a_values = array($a_values);
        }
        if (!is_array($a_keys)) {
            $a_keys = array($a_keys);
        }

        $i_num = count($a_fields);
        $j = 0;
        for ($i = 0; $i < $i_num; $i++) {
            (array_key_exists($i, $a_keys) && !empty($a_keys[$i])) ? $s_key = $a_keys[$i] : $s_key = '=';

            $this->addField($a_fields[$i], $a_types[$j], $a_values[$j], $s_key, 'AND');
            if ($s_key == 'BETWEEN') {
                $j++;
                $this->a_types[] = $a_types[$j];
                $this->a_values[] = $a_values[$j];
            }
            $j++;
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addOr($a_fields, $a_types, $a_values, $a_keys)
    {
        trigger_error('Class QueryConditions_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        if (!is_array($a_fields)) {
            $a_fields = array($a_fields);
            $a_types = array($a_types);
            $a_values = array($a_values);
            $a_keys = array($a_keys);
        }

        $i_num = count($a_fields);
        $j = 0;
        for ($i = 0; $i < $i_num; $i++) {
            (array_key_exists($i, $a_keys) && !empty($a_keys[$i])) ? $s_key = $a_keys[$i] : $s_key = '=';

            $this->addField($a_fields[$i], $a_types[$j], $a_values[$j], $s_key, 'OR');
            if ($s_key == 'BETWEEN') {
                $j++;
                $this->a_types[] = $a_types[$j];
                $this->a_values[] = $a_values[$j];
            }
            $j++;
        }

        return $this;
    }

    /**
     * Adds a field
     *
     * @param        String $s_field The field
     * @param        array $s_type The value type : l (SQL, no parse), i (int) ,d (double) ,s (string) or b (blob), also accepts a single value
     * @param        String $s_value The value
     * @param        String $s_key The key (=|<>|<|>|LIKE|IN|BETWEEN)
     * @param        String $s_command The command (AND|OR)
     * @throws DBException        If the key is invalid
     */
    protected function addField($s_field, $s_type, $s_value, $s_key, $s_command)
    {
        if (!array_key_exists($s_key, $this->a_keys)) {
            throw new DBException('Unknown where key ' . $s_key . '.');
        }

        if (!empty($this->s_query)) {
            $this->s_query .= ' ' . $s_command . ' ';
        }

        if ($s_type == 'l') {
            if ($s_key != 'IN') {
                $this->s_query .= $s_field . ' ' . $this->a_keys[$s_key] . ' ' . $s_value . ' ';
            } else {
                $this->s_query .= $s_field . ' IN (' . $s_value . ') ';
            }

            return;
        } else {
            if ($s_key == 'IN') {
                $a_data = array();

                foreach ($s_value AS $item) {
                    switch ($s_type) {
                        case 'i' :
                            if (!is_int($item)) {
                                throw new DBException('Invalid IN value. expected int but got ' . gettype($item) . '.');
                            }
                            $a_data[] = $item;
                            break;

                        case 'i' :
                            if (!is_float($item)) {
                                throw new DBException('Invalid IN value. expected float but got ' . gettype($item) . '.');
                            }
                            $a_data[] = $item;
                            break;

                        case 'i' :
                            if (!is_string($item)) {
                                throw new DBException('Invalid IN value. expected string but got ' . gettype($item) . '.');
                            }
                            $a_data[] = "'" . $item . "'";
                            break;


                        default :
                            throw new DBException('Invalid IN type. Only ints, floats and strings are supported.');
                    }
                }

                $this->s_query .= $s_field . ' IN (' . implode(',', $a_data) . ') ';
                return;
            }
        }

        $this->a_types[] = $s_type;
        $this->a_values[] = $s_value;

        if ($s_key == 'LIKE') {
            $this->s_query .= $s_field . ' LIKE CONCAT("%",?,"%") ';
        } else {
            if ($s_key == 'BETWEEN') {
                $this->s_query .= $s_field . ' BETWEEN ? AND ? ';
            } else {
                $this->s_query .= $s_field . ' ' . $this->a_keys[$s_key] . ' ? ';
            }
        }
    }
}