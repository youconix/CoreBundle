<?php

namespace youconix\Core\Database;

/**
 * @deprecated
 */
class Having_Mysqli extends QueryConditions_Mysqli implements Having
{
    /**
     * @inheritdoc
     */
    public function startSubHaving()
    {
        trigger_error('Class Having_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        $this->s_query .= '(';

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function endSubHaving()
    {
        trigger_error('Class Having_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        $this->s_query .= ')';

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function render()
    {
        trigger_error('Class Having_Mysqli is deprecated. Use Doctrine', E_DEPRECATED);

        if (empty($this->s_query)) {
            return null;
        }

        return array('having' => ' HAVING ' . $this->s_query, 'values' => $this->a_values, 'types' => $this->a_types);
    }
}