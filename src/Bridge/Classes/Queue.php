<?php

namespace youconix\Core\Bridge\Classes;

/**
 * Class Queue
 * @package youconix\Core\Bridge\Classes
 * @deprecated
 */
class Queue extends \SplQueue
{
    /**
     * @param array $content
     */
    public function __construct($content = [])
    {
        $this->setIteratorMode(\SplDoublyLinkedList::IT_MODE_FIFO | \SplDoublyLinkedList::IT_MODE_KEEP);
        $this->clear();
        $this->addArray($content);
    }

    /**
     * Merges the given queue with this one
     *
     * @param Queue $queue
     * @deprecated
     */
    public function addQueue(Queue $queue)
    {
        trigger_error('Class Queue is deprecated. Use \SplQueue', E_DEPRECATED);

        while ($queue->valid()) {
            $this->push($queue->pop());
            $queue->next();
        }
    }


    /**
     * Adds the array to the stack
     *
     * @param array $content
     * @deprecated
     */
    public function addArray(array $content)
    {
        trigger_error('Class Queue is deprecated. Use \SplQueue', E_DEPRECATED);

        foreach ($content as $item) {
            $this->push($item);
        }
    }

    /**
     * Searches if the stack contains the given item
     *
     * @param Object $search The item
     * @return bool    True if the queue contains the item
     * @deprecated
     */
    public function search($search)
    {
        trigger_error('Class Queue is deprecated. Use \SplQueue', E_DEPRECATED);

        $found = false;
        $position = $this->key();
        $this->rewind();
        while ($this->valid()) {
            if ($this->current() == $search) {
                $found = true;
                break;
            }
            $this->next();
        }
        $this->rewind();
        while ($this->valid()) {
            if ($this->key() === $position) {
                break;
            }
        }
        return $found;
    }

    /**
     * Clears the stack
     * @deprecated
     */
    public function clear()
    {
        trigger_error('Class Queue is deprecated. Use \SplQueue', E_DEPRECATED);

        $this->rewind();
        while ($this->valid()) {
            $this->pop();
            $this->next();
        }
        $this->rewind();
    }
}