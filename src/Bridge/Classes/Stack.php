<?php

namespace youconix\Core\Bridge\Classes;

/**
 * @deprecated
 */
class Stack extends \SplStack
{
    /**
     * @param array $content
     */
    public function __construct($content = [])
    {
        $this->setIteratorMode(\SplDoublyLinkedList::IT_MODE_LIFO | \SplDoublyLinkedList::IT_MODE_KEEP);
        $this->clear();
        $this->addArray($content);
    }

    /**
     * Merges the given stack with this one
     *
     * @param Stack $stack
     * @deprecated
     */
    public function addStack(Stack $stack)
    {
        trigger_error('Class Stack is deprecated. Use \SplStack', E_DEPRECATED);

        while ($stack->valid()) {
            $this->push($stack->pop());
            $stack->next();
        }
    }

    /**
     * Adds the array to the stack
     *
     * @param array $content
     * @deprecated
     */
    public function addArray(array $content)
    {
        trigger_error('Class Stack is deprecated. Use \SplStack', E_DEPRECATED);

        foreach ($content as $item) {
            $this->push($item);
        }
    }

    /**
     * Searches if the stack contains the given item
     *
     * @param Object $search The item
     * @return Boolean    True if the queue contains the item
     */
    public function search($search)
    {
        trigger_error('Class Stack is deprecated. Use \SplStack', E_DEPRECATED);

        $found = false;
        $position = $this->key();
        $this->rewind();
        while ($this->valid()) {
            if ($this->current() == $search) {
                $found = true;
                break;
            }
            $this->next();
        }
        $this->rewind();
        while ($this->valid()) {
            if ($this->key() === $position) {
                break;
            }
        }
        return $found;
    }

    /**
     * Clears the stack
     * @deprecated
     */
    public function clear()
    {
        trigger_error('Class Stack is deprecated. Use \SplStack', E_DEPRECATED);

        $this->rewind();
        while ($this->valid()) {
            $this->pop();
            $this->next();
        }
        $this->rewind();
    }
}