<?php

namespace youconix\Core\Bridge\Exceptions;

/**
 * @deprecated
 */
class TemplateException extends GeneralException {
    public function __construct($s_message){
        parent::__construct($s_message);
    }
}
